import xerial.sbt.Pack._
packAutoSettings

/* Data */
organization := "mx.unam.ciencias.hoc"
name := "tcp-simulated-annealing"
version := "1.0"
scalaVersion := "2.11.8"
/* Settings */
exportJars := true
/* Dependencies */
libraryDependencies ++= Seq(
  "com.typesafe.slick" %% "slick" % "3.1.1",
  "org.xerial" % "sqlite-jdbc" % "3.7.2",
//  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "org.scala-graph" %% "graph-core" % "1.11.2",
  "org.scalactic" %% "scalactic" % "3.0.0",
  "org.scalatest" %% "scalatest" % "3.0.0" % "test",
  "ch.qos.logback" %  "logback-classic" % "1.1.7",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0",
  "com.github.scopt" %% "scopt" % "3.5.0"
)

unmanagedBase <<= baseDirectory { base => base / "lib" }

fork in (run) := true
