#!/bin/zsh
gnuplot <<- EOF
        set key off
        set size ratio 0.3
        set xlabel "Iterations"
        set ylabel "Cost function"
        set title "Simulated Annealing"
        set term svg background rgb 'white'
        set output "$1.svg"
        plot "$1" with lines
EOF
inkscape "$1".svg
