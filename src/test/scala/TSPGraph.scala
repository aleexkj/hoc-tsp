import org.scalatest._
import scala.collection.mutable.ListBuffer
import Constants._

class TSPGraphTest extends FlatSpec with Matchers {
  val map = new WorldGraph()
  val city1 = new City((1, "city_1", "country_1", 10, 10, 9.9))
  val city2 = new City((2, "city_2", "country_2", 10, 10, 9.9))
  val city3 = new City((3, "city_3", "country_3", 10, 10, 9.9))

  "addNode" should "do instertion to graph" in {
    map.addNode((1, "city_1", "country_1", 10, 10, 9.9))
    val node = map.getCityNode(1)
    assert(node.id == 1)
    assert(node.name == "city_1")
    assert(node.country == "country_1")
    assert(node.population == 10)
    assert(node.latitude == 10)
    assert(node.longitude == 9.9)
  }

  it should "do insertion with city" in {
    map.addNodeCity(city2)
    val node = map.getCityNode(2)
    assert(node.id == 2)
    assert(node.name == "city_2")
    assert(node.country == "country_2")
    assert(node.population == 10)
    assert(node.latitude == 10)
    assert(node.longitude == 9.9)
  }

  "addEdge" should "create edge for row" in {
    map.addEdge((1, 2, 43.5))
    assert(map.graph.edges.length == 1)
  }

  it should "create edge for cities" in {
    map.addNodeCity(city3)
    map.addEdgeCity(city2, city3, 10)
    assert(map.graph.edges.length == 2)
  }

  "subGraph" should "return valid subgraph" in {
    val g = map.subGraph(Set[Int](1, 2))
    val node1 = g find 1
    val node2 = g find 2

    assert(node1 != None)
    assert(node2 != None)
    assert(g.edges.length == 1)
  }
}
