import org.scalatest._
import scala.collection.mutable.ListBuffer
import Constants._

class PathTest extends FlatSpec with Matchers {
  val map = new WorldGraph()
  map.addNode((1, "city_1", "country_1", 10, 10, 9.9))
  map.addNode((2, "city_2", "country_2", 10, 10, 9.9))
  map.addNode((3, "city_3", "country_3", 10, 10, 9.9))
  map.addNode((4, "city_4", "country_4", 10, 10, 9.9))
  map.addEdge((1, 2, 43.5))
  map.addEdge((2, 3, 65.5))
  map.addEdge((3, 1, 7.9))

  /* Factories */
  var smap = map.subGraph(Set[Int](1, 2, 3, 4))

  var cities = Array[Int](1, 2, 3)
  val connectedPath = new Path(cities)

  cities = Array[Int](1, 2, 4)
  val disconnectedPath = new Path(cities)

  cities = Array[Int](4, 1, 3)
  val disconnectedPathB = new Path(cities)

  "neighbor" should "return a valid path" in {
    val npath = connectedPath.neighbor()
    assert(connectedPath.length == npath.length)
    /* Count distinct elements */
    var distinct = 0
    val index = ListBuffer[Int]()

    for (i <- 0 until connectedPath.length) {
      if (connectedPath.get(i) != npath.get(i)) {
        distinct += 1
        index += i
      }
    }
    /* Check swap */
    assert(distinct == 2)
    assert(connectedPath.get(index(0)) == npath.get(index(1)))
    assert(connectedPath.get(index(1)) == npath.get(index(0)))
  }

  "areConnected" should "return true when exist edge" in {
    assert(connectedPath.areConnected(1, 2))
  }

  it should "return false when there's no edge" in {
    assert(!disconnectedPath.areConnected(1, 4))
  }

  "isFactible" should "return true when path is connected" in {
    assert(connectedPath.isFactible())
  }

  it should "return false when two cities aren't connected" in {
    /* last city not connected */
    assert(!disconnectedPath.isFactible())
    /* first city not connected */
    assert(!disconnectedPathB.isFactible())
  }

  "distanceBetween" should "throw IllegalArgumentException for two cities disconnected" in {
    assertThrows[IllegalArgumentException] {
      disconnectedPath.distanceBetween(2, 4)
    }
  }

  it should "return the distance between two cities" in {
    val d = connectedPath.distanceBetween(2, 1)
    assert(d == 43.5)
  }

  "cost" should "return the sum of the distances" in {
    val d = connectedPath.cost()
    assert(d == (43.5 + 65.5) / (TSPCommon.upper * (connectedPath.length - 1)))
  }

  "companion object" should "reset the random generator" in {
    TSPCommon.setRandom(10)
    val a = TSPCommon.nextRandom(10)
    TSPCommon.reloadRandom()
    assert(TSPCommon.nextRandom(10) == a)
  }
}
