import org.scalatest._
import Constants._
import java.lang.Math._

class SimulatedAnnealingTest extends FunSpec with Matchers {
  val map = new WorldGraph()
  map.addNode((1, "city_1", "country_1", 10, 10, 9.9))
  map.addNode((2, "city_2", "country_2", 10, 10, 9.9))
  map.addNode((3, "city_3", "country_3", 10, 10, 9.9))
  map.addNode((4, "city_4", "country_4", 10, 10, 9.9))
  map.addNode((5, "city_5", "country_5", 10, 10, 9.9))
  map.addEdge((1, 2, 43.5))
  map.addEdge((2, 3, 65.5))
  map.addEdge((3, 1, 7.9))

  /* Factories */
  val smap = map.subGraph(Set[Int](1, 2, 3, 4, 5))
  TSPCommon.setMap(smap)

  var cities = Array[Int](1, 2, 3)
  val connectedPath = new Path(cities)

  cities = Array[Int](3, 1, 2)
  val connectedPathB = new Path(cities)

  cities = Array[Int](1, 5, 4)
  val disconnectedPath = new Path(cities)

  val dummy = (x: Double) => {}

  describe("Batch") {
    describe("calculate") {
      it("should terminate with appropiate path") {
        val t = new Temperature(2.0)
        val b = new Batch(2, 10) // only two neighbors are better
        val (avg, s, best, c) = b.calculate(connectedPath, t, dummy)
        assert(avg > 0.0)
        assert(s.cost() <= connectedPath.cost()) // both have to be better
        assert(best.cost() <= connectedPath.cost())
        assert(best.cost() <= s.cost()) // maybe the best was the last
      }
    }
  }

  describe("Temperature") {
    describe("methods") {
      it("should return next temperature value") {
        val t = new Temperature(2.0)
        val next = t.value * PHI
        assert(t.next() == next)
        assert(t.value == next)
      }

      it("should return the current value") {
        val t = new Temperature(4.0)
        assert(t.value == 4.0)
      }
    }

    describe("companion object") {
      it("should return valid percent") {
        val p = Temperature.acceptedPercent(connectedPath, 10)
        assert(p >= 0 && p <= 1.0)
      }
    }
  }
}
