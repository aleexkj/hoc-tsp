import EntitiesTypes._
import GraphTypes._
import Constants._
import WeightImplicit._
import com.typesafe.scalalogging.Logger

import scala.util.Random

/**
 * Represents a city
 */
class City(
  val id: Int,
  val name: String,
  val country: String,
  val population: Int,
  val latitude: Double,
  val longitude: Double){

  /**
   * Creates a City given a tuple with params
   * @param [[EntitiesTypes.CityRow]]
   */
  def this(params: CityRow) = this(
    params._1,
    params._2,
    params._3,
    params._4,
    params._5,
    params._6)

  /* Get geographic coordinates */
  def x = longitude.toFloat
  def y = latitude.toFloat

}

/**
 * Contains common objects for paths, such as
 * the map they live in and random generator
 */
object TSPCommon {
  var r = new Random()
  var tspGraph: Option[TSPGraph] = None
  var upper: Double = MAXDISTANCE
  var seed: Option[Long] = None
  val logger = Logger("hoc-tsp:TSPCommon")

  /**
   * Generates new random for TSP
   * @param  {Int} limit: Int           Upper bound
   * @return {Int}        Random in range: [0, limit)
   */
  def nextRandom(limit: Int): Int = {
    return r.nextInt(limit)
  }

  /**
   * Reset the random number generator
   */
  def reloadRandom() {
    seed match {
      case Some(s) => r = new Random(s)
      case None => r = new Random()
    }
  }

  def setRandom(s: Long) {
    seed = Some(s)
    reloadRandom()
  }

  /**
   * Set the world graph for TSP solutions
   * @param {[[TSPGraph]]} tspMap: TSPGraph Graph with cities and connections
   */
  def setMap(tspMap: TSPGraph) {
    tspGraph = Some(tspMap)
  }

  /**
   * Getter for the map
   * @type {TSPGraph}
   */
  def map: TSPGraph = tspGraph match {
    case Some(s) => s
    case None => throw new NoSuchElementException()
  }

  def setUpper() {
    var d = 0.0

    tspGraph match {
      case Some(g) =>
      for (edge <- g.edges){
        if (edge.dweight > d)
          d = edge.dweight
      }

      upper = d
      logger.debug(s"${upper} is the max distance between two cities")

      case None => throw new NoSuchElementException()
    }
  }
}

/**
 * Represents a Path of k cities
 * @param kCities: array representing the order to travel
 */
class Path(kCities: Array[Int]) extends Solution {
  val k = kCities.length // shortcut

  /**
   * Computes a neighbor solution resulting for cities swap
   * @return {Path} Solution with two cities swapped
   */
  override def neighbor(): Path = {
    val i = TSPCommon.nextRandom(k)
    val j = TSPCommon.nextRandom(k)

    if (i == j) // try again
      return neighbor()

    val swap = Array.tabulate[Int](k)(n => n match {
      case x if x == i => kCities(j)
      case x if x == j => kCities(i)
      case x: Int => kCities(x)
    })

    return new Path(swap)
  }

  /**
   * Returns true when the path is connected
   * false otherwise
   */
  override def isFactible(): Boolean = {
    for (i <- 0 until k - 1) {
      if (!areConnected(kCities(i), kCities(i + 1)))
        return false
    }

    return true
  }

  /**
   * Returns the cost of this path
   * := sum of the distances between cities in the order of kCities
   */
  override def cost(): Double = {
    var total: Double = 0.0
    for (i <- 0 until k - 1) {
      var d: Double = 0.0
      try {
        d = distanceBetween(kCities(i), kCities(i + 1))
      } catch {
        case e: IllegalArgumentException => d = 10.0 * TSPCommon.upper
        case e: Exception => throw new RuntimeException("Can't calculate distance")
      }
      total += d
    }

    return total / (TSPCommon.upper * (k - 1.0))
  }

  /**
   * Test if two cities are connected in map
   */
  def areConnected(a: Int, b: Int): Boolean = {
    val aNode = TSPCommon.map get a
    for (n <- aNode.neighbors) {
      if (n == b) {
        return true
      }
    }
    println(s"$a and $b aren't connected")
    return false
  }

  /**
   * Return the distance between two cities a, b
   * throws exception if no edge exists
   */
  def distanceBetween(a: Int, b: Int): Double = {
    val node = TSPCommon.map get a

    for (e <- node.incoming) {
      if (e._1 == b || e._2 == b)
        return e.dweight
    }

    throw new IllegalArgumentException("cities aren't connected")
  }

  /* utils */
  def length = kCities.length
  def get(i: Int) = kCities(i)
  def getCities(): Array[Int] = kCities

  override def toString = kCities.mkString(", ")
}
