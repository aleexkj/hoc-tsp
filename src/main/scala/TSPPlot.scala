import processing.core._
import processing.event.MouseEvent
import grafica._
import scala.util.Random

class TSPPlot extends PApplet {
  val plot = new GPlot(this)
  val points = new GPointsArray()

  var i = 0

  val WIDTH = 500
  val HEIGHT = 500
  val TEXTSIZE = 16

  /**
   * Runs before the sketch has been set up
   */
  override def settings() {
     size(WIDTH, HEIGHT)

     plot.setOuterDim(WIDTH, HEIGHT)
     plot.setPos(0, 0)
     plot.getXAxis().getAxisLabel().setText("Iterations")
     plot.getYAxis().getAxisLabel().setText("Fitness")
     plot.setTitleText("Cost function over time")
     plot.setPoints(new GPointsArray())
     plot.setPointColor(color(100, 100, 255, 80))
     plot.setLineColor(color(100, 100, 200))
  }

  /**
   * Runs when the app starts, define intial enviroment
   */
  override def setup() {
    val font: PFont = createFont("OpenSans.ttf", TEXTSIZE)
    textFont(font)
    textSize(TEXTSIZE)
  }

  /**
   * Runs 60fps
   */
  override def draw() {
    plot.beginDraw()
    plot.drawBackground()
    plot.drawBox()
    plot.drawXAxis()
    plot.drawYAxis()
    plot.drawTopAxis()
    plot.drawRightAxis()
    plot.drawTitle()
    plot.drawGridLines(GPlot.BOTH)
    plot.drawLines()
    //plot.drawPoints()
    plot.endDraw()
  }

  def addPoint(cost: Double) {
    plot.addPoint(i, cost.toFloat)
    i += 1
  }

  /* Launch app */
  def run() {
    PApplet.runSketch(Array[String]("Travelling Salesman Problem"), this)
  }
}
