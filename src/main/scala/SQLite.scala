import slick.driver.SQLiteDriver.api._
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{Future, Await}
import org.slf4j.LoggerFactory
import ch.qos.logback.classic.{Level, Logger}

import EntitiesTypes._

/**
 * Retrieves the cities and connection from a SQLite3 database
 */
object SQLiteTSP {
  // Turn off logger of slick
  val slogger = LoggerFactory.getLogger("slick").asInstanceOf[Logger]
  slogger.setLevel(Level.INFO)

  def loadDatabase(cbCity: CityRow => Unit, cbConnection: ConnectionRow => Unit, done: => Unit)= {
    val db = Database.forURL("jdbc:sqlite:db/hoc-tsp.db", driver = "org.sqlite.JDBC")
    val cities = TableQuery[Cities] // Interfaces for querying
    val connections = TableQuery[Connections]

    slogger.info("Loading database 'db/hoc-tsp.db'")

    val nodes: Future[Seq[CityRow]] = db.run(cities.result) // Async for ``SELECT * FROM cities``
    val edges: Future[Seq[ConnectionRow]] = nodes flatMap { // Once the cities are ready, get edges
      rows =>
        rows.foreach(cbCity) // Map each row to cbCity(CityRow)
        db.run(connections.result) // Async for ``SELECT * FROM connections``
    }

    val rows = Await.result(edges, Duration.Inf) // Waits for the result
    rows foreach cbConnection // Map result to cbConnection(ConnectionRow)

    db.close // close database

    slogger.info("Done.")
    done // db was read
  }
}
