import processing.core._
import processing.event.MouseEvent
import org.gicentre.geomap._
import scala.collection.mutable.HashMap

/**
 * Definition of processing sketch
 */
class TSPApp(worldGraph: WorldGraph, path: Set[Int])
 extends PApplet
{

  /**
   * Size definitions
   */
   val WIDTH = 900
   val HEIGHT = 550
   val DOCKSTART = HEIGHT + 3
   val DOCKHEIGHT = 40
   val TEXTSIZE = 14

  /**
   * GeoMap object
   */
  val worldMap = new GeoMap(0, 0, WIDTH, HEIGHT, this)

  /**
   * Zoomer util
   */
  val zoomer = new PanZoomController(this)

  /**
   * Result path
   */
  var result = Array[Int]()
  var drawRes = false

  /**
   * To get country coordinates
   */
  val coordinates = HashMap[Int, PVector]()

  /**
   * Colors
   */
  val waterColor = color(0, 28, 43)
  val landColor = color(19, 72, 74)
  val borderColor = color(127, 118, 106)
  val hoverColor = color(114, 197, 167) // resalts
  val clearColor = color(277, 255, 226) // replace white
  val cityColor = color(216, 90, 16) // cities visiting

  /**
   * Runs before the sketch has been set up
   */
  override def settings() {
     size(WIDTH, HEIGHT + DOCKHEIGHT)
     smooth()
     worldMap.readFile("world")
     zoomer.setScaleVelocity(0.4f)
  }

  /**
   * Runs when the app starts, define intial enviroment
   */
  override def setup() {
    val font: PFont = createFont("JosefinSans-Regular.ttf", TEXTSIZE)
    textFont(font)
    textSize(TEXTSIZE)

    for(city <- worldGraph.cities) {
      val coords: PVector = worldMap.geoToScreen(city.x, city.y)
      coordinates += city.id -> coords
    }
  }

  /**
   * Runs 60fps
   */
  override def draw() {
    /* Transformations from mouse events */
    pushMatrix()
    translate(zoomer.getPan().x, zoomer.getPan().y)
    scale(zoomer.getScale())

    /* draw map */
    background(waterColor)
    stroke(borderColor)
    strokeWeight(0.2f)
    fill(landColor)
    worldMap.draw()

    /* draw cities */
    for (t <- coordinates) {
      val coords = t._2
      val city_id = t._1
      fill(hoverColor)
      noStroke()
      ellipse(coords.x, coords.y, 3, 3)
    }

    /* draw graph for search */
    val searchSpace = worldGraph.subGraphCity(path)
    for(city <- searchSpace.nodes) {
      // Node
      val coords: PVector = coordinates(city.id)
      fill(cityColor)
      ellipse(coords.x, coords.y, 5, 5)
      fill(clearColor)
      textSize(8)
      text(s"${city.id}", coords.x + 5, coords.y - 5)
    }

    /* draw solution */
    if(drawRes) {
      noFill()
      stroke(clearColor)
      beginShape()
      for(id <- result) {
        val coords: PVector = coordinates(id)
        vertex(coords.x, coords.y)
      }
      endShape()
    }

    /* end of transformated plane */
    popMatrix()

    /* draw dock */
    noStroke()
    textSize(TEXTSIZE)
    fill(50)
    rect(0, HEIGHT, WIDTH, DOCKSTART)
    fill(200)
    rect(0, DOCKSTART, WIDTH, HEIGHT + DOCKHEIGHT)
    fill(33)
    var tstart = DOCKSTART + TEXTSIZE + 3
    text("Travelling Salesman Problem", 10, tstart)
    tstart += TEXTSIZE + 3
    text(s"${worldGraph.cities.length} cities loaded", 10, tstart)
  }

  /**
   * Listeners for Zooming and Panning
   * required for PanZoomController
   */
  override def keyPressed() {
    zoomer.keyPressed()
    if (key == 'p' || key == 'P') {
      drawRes = !drawRes
    }
  }

  override def mouseDragged() {
    zoomer.mouseDragged()
  }

  override def mouseWheel(event: MouseEvent) {
    zoomer.mouseWheel(event.getCount())
  }

  /* Launch app */
  def run() {
    PApplet.runSketch(Array[String]("Travelling Salesman Problem"), this)
  }

  /* draw result */
  def drawResult(r: Array[Int]) {
    result = r
    drawRes = true
  }
}
