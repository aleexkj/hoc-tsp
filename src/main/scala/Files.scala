import scala.io.Source
import java.io._
import scala.util.Random

object Files {
  def read(filename: String, separator: String = ", "): List[Int] = {
    var file_string = ""

    try {
      val buffer = Source.fromFile(filename)
      file_string = buffer.getLines.mkString
      buffer.close
    } catch {
      case e: Exception => return List()
    }

    var cities = file_string.split(separator)
    var cities_ids = new Array[Int](cities.length)
    for (i <- 0 until cities.length) {
      cities_ids(i) = cities(i).toInt
    }

    val shuffled_ids = Random.shuffle(cities_ids.toList)

    return shuffled_ids
  }

  def openForWrite(filename: String): PrintWriter = {
    val writer = new PrintWriter(new File(filename))
    return writer
  }

  def readSolo(filename: String, separator: String = ", "): List[Int] = {
    var file_string = ""

    try {
      val buffer = Source.fromFile(filename)
      file_string = buffer.getLines.mkString
      buffer.close
    } catch {
      case e: Exception => return List()
    }

    var cities = file_string.split(separator)
    var cities_ids = new Array[Int](cities.length)
    for (i <- 0 until cities.length) {
      cities_ids(i) = cities(i).toInt
    }

    return cities_ids.toList
  }
}
