/**
 * Represents the config parameters of the project
 */
case class Config(input: Option[String] = None,
  output: Option[String] = None,
  gui: Boolean = false,
  plot: Boolean = false,
  seed: Long = 0,
  temperature: Int = 80,
  binaryTemperature: Boolean = false,
  size: Option[Int] = Some(15))

/**
 * Args parser, builds and validates the args passed to the program
 */
class GetOpts() extends scopt.OptionParser[Config]("main") {
  head("tsp-hoc version 1.0 (c) kreiz")
  help("help").text("Show help")
  version("version").text("Show version")

  opt[String]('i', "input")
  .valueName("<input_file>")
  .text("Input file if not specified it will use random option")
  .action((x, c) => c.copy(input = Some(x), size = None))

  opt[Int]('r', "random")
  .valueName("<n>")
  .text("Generate n random cities. Overrides input option. Default: 15")
  .action((x, c) => c.copy(input = None, size = Some(x)))
  .validate(x => if (x > 0) success else failure("Positive ingeger needed"))

  opt[Unit]('b', "binary-seach-temperature")
  .optional()
  .text("Indicates if temperature is pre-calculated")
  .action((_, c) => c.copy(binaryTemperature=true))

  opt[String]('o', "output")
  .valueName("<output_file>")
  .text("Write accepted solutions to a file")
  .action((x, c) => c.copy(output = Some(x)))

  opt[Unit]('g', "gui")
  .optional()
  .text("Wether to show GUI or not")
  .action((_, c) => c.copy(gui=true))

  opt[Unit]('p', "plot")
  .optional()
  .text("Show plot of accepted solutions")
  .action((_, c) => c.copy(plot=true))

  arg[Int]("<seed>")
  .required()
  .text("RNG seed")
  .action((x, c) => c.copy(seed = x))

  arg[Int]("<initial>")
  .required()
  .text("Initial temperature")
  .action((x, c) => c.copy(temperature = x))

  override def showUsageOnError = true
}

// Constants for simulated annealing
object Constants {
  val PHI = 0.9 // how temperature decreases
  val MAXDISTANCE: Double = 384400000.0 // moon-earth distance (in meters)
  val MAXABORTED: Int = 1000
  val ETHERMAL = 0.05 // thermal equilibrium (batches acceptation)
  val ETEMPERATURE = 0.0001 // final temperature difference
  val BATCHSIZE: Int = 400
  val PACCEPT = 0.8 // percent of sol. to accept with initial temperature
  val EPERCENT = 0.01 // epsilon for acceptig percents
  val EPTEMPERATURE = ETEMPERATURE // difference for temperatures in binary search
  val NPERCENT: Int = BATCHSIZE * 10 // sample to initial temperature
}
