import scalax.collection.mutable.Graph
import scalax.collection.edge.LUnDiEdge
import scalax.collection.edge.Implicits._
import scalax.collection.edge.LBase.LEdgeImplicits

import EntitiesTypes._

/**
 * Definitions for double weighted edges
 */
case class DoubleWeight(dweight: Double)
object WeightImplicit extends LEdgeImplicits[DoubleWeight]
import WeightImplicit._

/**
 * Definitions for graph utils
 */
object GraphTypes {
  type TSPGraph = Graph[Int, LUnDiEdge]
  type TSPGraphCity = Graph[City, LUnDiEdge]
}

/**
 * Represents the database as a graph
 * cities - the list of cities available
 * graph - the graph
 *
 *
 * graph is an attribute 'cause extending would be messy
 */
class WorldGraph() {


  /* Graph */
  val graph = Graph[City, LUnDiEdge]()
  // Accessor
  def cities = graph.nodes
  def connections = graph.edges

  /**
   * Add node to graph (id of city) and city to list
   */
  def addNode(row: CityRow) {
    addNodeCity(new City(row))
  }

  def addNodeCity(city: City) {
    graph += city
  }

  /**
   * Add an edge between two cities with distance=weigth
   */
  def addEdge(row: ConnectionRow) {
    val city1 = getCityNode(row._1)
    val city2 = getCityNode(row._2)
    addEdgeCity(city1, city2, row._3)
  }

  def addEdgeCity(city1: City, city2: City, weight: Double) {
    val edge = (city1 ~+ city2)(DoubleWeight(weight))
    graph += edge
  }

  /**
   * Computes de subgraph of the map (only cities indices)
   * @param  {Set[Int]} path: Set[Int]      nodes cities ids of subgraph
   * @return {GraphTypes.TSPGraph}       resulting subgraph
   */
  def subGraph(path: Set[Int]): GraphTypes.TSPGraph = {
    val sg = subGraphCity(path)
    val g = Graph[Int, LUnDiEdge]()

    for (node <- sg.nodes) {
      g += node.id
    }

    for (edge <- sg.edges) {
      g += (edge._1.id ~+ edge._2.id)(DoubleWeight(edge.dweight))
    }

    return g
  }

  def subGraphCity(path: Set[Int]): GraphTypes.TSPGraphCity = {
    val subgraph = graph filter graph.having(node = (n) => path(n.id))
    return subgraph
  }

  /**
   * gets the node of the city with given id
   * @param  {Int} id: Int           the id to searchSpace
   * @return {City}     city found in graph
   */
  def getCityNode(id: Int): City = {
    for (n <- graph.nodes) {
      if (n.id == id) {
        return n
      }
    }

    throw new NoSuchElementException()
  }

  def listOfIds(): List[Int] = {
    val ids = graph.nodes.map(x => x.id)
    return ids.toList
  }
}
