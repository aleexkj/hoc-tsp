import scala.collection.mutable.ListBuffer
import Constants._
import java.lang.Double.POSITIVE_INFINITY
import java.lang.Math._
import com.typesafe.scalalogging.Logger
/**
 * Represents a solution for an heuristic
 */
abstract class Solution {
  def neighbor(): Solution
  def isFactible(): Boolean
  def cost(): Double
}

/**
 * Represents the temperature
 * @param {Double} var initial: Double initial temperature
 */
class Temperature(var initial: Double) {
  val logger = Logger("hoc-tsp: Temperature")

  def next(): Double = {
    initial = initial * PHI
    return initial
  }

  def value = initial
}

object Temperature {

  /**
   * Creates a new Temperature calculating
   * the percent of accepted solutions at
   * the start s
   */
  def apply(t: Double, s: Solution): Temperature = {
    var p: Double = acceptedPercent(s, t)

    if (abs(PACCEPT - p) <= EPERCENT) { // when accepted percent is accurate
      return new Temperature(t)
    }

    var t1: Double = 0.0
    var t2: Double = 0.0
    var t0: Double = t

    if (p < PACCEPT) {
      while (p < PACCEPT) { // duplicate temperature until acceptedPercent is accurate
        t0 = 2.0 * t0
        p = acceptedPercent(s, t0)
      }

      t1 = t0 / 2.0 // range for search good temperature
      t2 = t0
    } else {
      while (p > PACCEPT) { // inverse
        t0 = t0 / 2.0
        p = acceptedPercent(s, t0)
      }

      t1 = t0
      t2 = 2.0 * t0
    }

    return binarySearch(s, t1, t2) // look in the range (t1, t2) a good temperature
  }

  /**
   * Given a solution and a temperature
   * calculates de percent of accepted solutions
   */
  def acceptedPercent(inital: Solution, t: Double): Double = {
    var c: Double = 0.0 // accepted counter
    var s: Solution = inital

    for (i <- 1 until NPERCENT) { // sample size
      var s2 = s.neighbor()
      var costS = s.cost()
      var costS2 = s2.cost()

      if (costS2 <= costS + t) {
        c += 1
      }

      s = s2
    }

    return c / NPERCENT.toDouble
  }

  /**
   * Given a range of temperatures and a solution
   * searchs for the best temperature that fits
   * accepted solutions wanted
   */
  def binarySearch(s: Solution, t1: Double, t2: Double): Temperature = {
    var tm: Double = (t1 + t2) / 2.0 // middle point

    if (abs(t2 - t1) < EPTEMPERATURE) {
      return new Temperature(tm)
    }

    var p: Double = acceptedPercent(s, tm)
    if (abs(PACCEPT - p) < EPERCENT) {
      return new Temperature(tm)
    }

    if (p > PACCEPT) {
      return binarySearch(s, t1, tm)
    } else {
      return binarySearch(s, tm, t2)
    }
  }
}

class AllNeighborsExploredException()
  extends RuntimeException(new RuntimeException("Can't find better neighbor"))

/**
 * Represents a batch for a SimulatedAnnealing run
 * @param {Int} size:    Int size of the batch (i.e. quant of accp sol)
 * @param {Int} maxIter: Int number of trials
 */
class Batch(size: Int, maxIter: Int) {
  type BatchResult = (Double, Solution, Solution, Double)

  val logger = Logger("hoc-tsp: Batch")

  def this(size: Int) = this(size, size * size)

  def calculate(solution: Solution, t: Temperature, cb: Double => Unit): BatchResult = {
    var best: Solution = solution // best seen so far
    var bestCost: Double = solution.cost()

    var s: Solution = solution // current solution
    var c: Double = 0.0 // accepted
    var g = 0 // gen solutions
    var avg: Double = 0.0 // avg of costFunction

    while (c < size && g <= maxIter) {
      val s2: Solution = s.neighbor()
      val costS: Double = s.cost()
      val costS2: Double = s2.cost()

      if (costS2 <= costS + t.value) { // accept
        s = s2
        c += 1
        avg += costS2

        if (costS2 < bestCost) { // best?
          best = s2
          bestCost = costS2
        }

        cb(costS2)
      }

      g += 1
    }

    if (c < size) {
      logger.debug(s"Can't complete batch, returning current state, accepted: $c")
    }

    return (avg / c, s, best, c)
  }
}

/**
 * SimulatedAnnealing heuristic
 * @param  {Temperature} t:  Temperature   initial Temperature
 * @param  {A<:Solution} var s:            A             initial solution
 * @return {[type]}     [description]
 */
class SimulatedAnnealing[A<:Solution](t: Temperature, var s: A) {
  var bestSolution: A = s
  val batch = new Batch(BATCHSIZE)
  var batchesRej = 0
  var iterations = 0

  def run(cb: Double => Unit) {

    time {
      var p: Double = POSITIVE_INFINITY

      while (t.value > ETEMPERATURE && batchesRej < MAXABORTED) {
        var p2 = 0.0

        while (abs(p - p2) > ETHERMAL) {
          p2 = p
          val (bp, bs, bbest, c) = batch.calculate(s, t, cb)

          if (bestSolution.cost() >= bbest.cost() && bbest.isFactible()) { // update best
            bestSolution = bbest.asInstanceOf[A]
          }

          if (c < BATCHSIZE) {
            batchesRej += 1
          }
          // update according to batch results
          s = bs.asInstanceOf[A]
          p = bp

          iterations += 1
          if (iterations == 20) {
            println(f"Current best: ${bestSolution.cost()}, temperature: ${t.value}%.4f")
            iterations = 0
          }
        }
        t.next()
      }
    }
  }

  def result(): A = {
    return bestSolution
  }

  def time[R](block: => R): R = {
    val t0 = System.currentTimeMillis()
    val result = block    // call-by-name
    val t1 = System.currentTimeMillis()
    println("Elapsed time: " + (t1 - t0) + "ms")
    result
  }
}
