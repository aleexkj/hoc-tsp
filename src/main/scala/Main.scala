import com.typesafe.scalalogging.Logger
import scala.util.Random

object main {
  val logger = Logger("hoc-tsp")
  /* FLAGS */
  var RANDOM_BUILD = false

  def main(args: Array[String]) {
    val parser = new GetOpts()

    parser.parse(args, Config()) match {
      case Some(config) =>
        /* Load cities */
        var cities: List[Int] = buildCities(handleInput(config.input, config.size))
        val g = new WorldGraph()
        /* Load database */
        SQLiteTSP.loadDatabase(g.addNode, g.addEdge, {
          if (RANDOM_BUILD && g.cities.length >= cities.length) {
            val shuffle = Random.shuffle(g.listOfIds())
            cities = shuffle.take(cities.length)
          } else if(RANDOM_BUILD) {
            logger.error("Not enough cities to build the problem")
            sys.exit()
          }
        }) // now is blocking method
        logger.info(s"${g.connections.length} connections, ${g.cities.length} cities")
        logger.info(s"Problem for ${cities.length} cities")

        /* Validations */
        if (g.connections.length == 0 || g.cities.length == 0) {
          logger.error("No data available")
          sys.exit()
        }

        /* Initialize simulated annealing */
        val map = g.subGraph(cities.toSet)
        logger.info(s"Map for solution: ${map.nodes.length} nodes, ${map.edges.length} connections")
        if (!map.isConnected) {
          logger.error(s"The map is not connected, there's no suitable path")
          sys.exit()
        }
        // Global
        TSPCommon.setMap(map)
        TSPCommon.setRandom(config.seed)
        TSPCommon.setUpper()
        // Temperature
        val initialSol = new Path(cities.toArray)
        var t: Temperature = new Temperature(config.temperature)
        if (config.binaryTemperature) {
          logger.info("Calculating temperature..")
          t = Temperature(config.temperature, initialSol)
          TSPCommon.reloadRandom()
        }
        logger.info(s"Initial temperature: ${t.value}")
        logger.info(s"Initial solution: $initialSol")
        logger.info(s"Factible: ${initialSol.isFactible()}")
        // Initialize problem
        val simulated_annealing = new SimulatedAnnealing[Path](t, initialSol)
        /* RUUUUN! */
        val format = (f: Double) => "%.4f\n" format (f.toFloat)
        var callback = (x: Double) => {}
        // Callbacks defined by user
        config.output match {
          case Some(filename) =>
            val file = Files.openForWrite(filename)
            callback = (x: Double) => { file.write(format(x)) }
            if (config.plot) {
              val app = new TSPPlot()
              callback = (x: Double) => { file.write(format(x)); app.addPoint(x) }
              app.run()
            }
            simulated_annealing.run(callback)
            file.close()
          case None =>
            if (config.plot) {
              val app = new TSPPlot()
              callback = (x: Double) => { app.addPoint(x) }
              app.run()
            }
            simulated_annealing.run(callback)
        }
        // Solution
        val sol = simulated_annealing.result()
        logger.info(s"Result: $sol")
        logger.info(s"Cost: ${sol.cost()}")
        logger.info(s"Factible: ${sol.isFactible()}")
        // GUI
        if (config.gui) {
          val app = new TSPApp(g, initialSol.getCities().toSet)
          app.drawResult(sol.getCities())
          app.run()
        }
      case None => // GetOpts handle the usage text
    }
  }


  def handleInput(file: Option[String], size: Option[Int]): Either[String, Int] = {
    file match {
      case Some(filename) => return Left(filename)
      case None =>
        size match {
          case Some(n) => return Right(n)
          case None => logger.error(s"Input file or size needed"); sys.exit()
        }
    }
  }

  def buildCities(option: Either[String, Int]): List[Int] = {
    option match {
      case Left(filename) =>
        val cities = Files.read(filename)
        if (cities.length == 0) {
          logger.error(s"Can't read input file '$filename'")
          sys.exit()
        }
        return cities
      case Right(size) => RANDOM_BUILD=true; return List.tabulate(size)(_ => 0)
    }
  }
}
