import slick.driver.SQLiteDriver.api._

/**
 * Types of the rows
 */
object EntitiesTypes {
  type CityRow = (Int, String, String, Int, Double, Double)
  type ConnectionRow = (Int, Int, Double)
}

/**
 * Collection of cities
 */
class Cities(tag: Tag)
  extends Table[EntitiesTypes.CityRow](tag, "cities") {

  def id = column[Int]("id", O.PrimaryKey)
  def name = column[String]("name")
  def country = column[String]("country")
  def population = column[Int]("population")
  def latitude = column[Double]("latitude")
  def longitude = column[Double]("longitude")

  def * = (id, name, country, population, latitude, longitude)
}

/**
 * Collection of connections between Cities
 */
class Connections(tag: Tag)
  extends Table[EntitiesTypes.ConnectionRow](tag, "connections") {

  def idCity1 = column[Int]("id_city_1")
  def idCity2 = column[Int]("id_city_2")
  def distance = column[Double]("distance")

  def * = (idCity1, idCity2, distance)
}
