DROP TABLE IF EXISTS cities;
CREATE TABLE cities (
       id                INTEGER PRIMARY KEY,
       name              TEXT,
       country           TEXT,
       population        INTEGER,
       latitude          DOUBLE,
       longitude         DOUBLE
);

BEGIN TRANSACTION;

INSERT INTO cities VALUES (1,"Tokyo","Japan",31480498,35.685,139.751389);
INSERT INTO cities VALUES (2,"Shanghai","China",14608512,31.045556,121.399722);
INSERT INTO cities VALUES (3,"Bombay","India",12692717,18.975,72.825833);
INSERT INTO cities VALUES (4,"Karachi","Pakistan",11627378,24.9056,67.0822);
INSERT INTO cities VALUES (5,"Delhi","India",10928270,28.666667,77.216667);
INSERT INTO cities VALUES (6,"New Delhi","India",10928270,28.6,77.2);
INSERT INTO cities VALUES (7,"Manila","Philippines",10443877,14.6042,120.9822);
INSERT INTO cities VALUES (8,"Moscow","Russian Federation",10381288,55.752222,37.615556);
INSERT INTO cities VALUES (9,"Seoul","Korea",10323448,37.5985,126.9783);
INSERT INTO cities VALUES (10,"São Paulo","Brazil",10021437,-23.473293,-46.665803);
INSERT INTO cities VALUES (11,"Istanbul","Turkey",9797536,41.018611,28.964722);
INSERT INTO cities VALUES (12,"Lagos","Nigeria",8789133,6.453056,3.395833);
INSERT INTO cities VALUES (13,"Mexico","Mexico",8720916,19.434167,-99.138611);
INSERT INTO cities VALUES (14,"Jakarta","Indonesia",8540306,-6.174444,106.829444);
INSERT INTO cities VALUES (15,"New York","United States",8107916,40.7141667,-74.0063889);
INSERT INTO cities VALUES (16,"Kinshasa","Democratic Republic of the Congo",7787832,-4.3,15.3);
INSERT INTO cities VALUES (17,"Cairo","Egypt",7734602,30.05,31.25);
INSERT INTO cities VALUES (18,"Lima","Peru",7646786,-12.05,-77.05);
INSERT INTO cities VALUES (19,"Peking","China",7480601,39.928889,116.388333);
INSERT INTO cities VALUES (20,"London","United Kingdom",7421228,51.514125,-0.093689);
INSERT INTO cities VALUES (21,"Bogotá","Colombia",7102602,4.649178,-74.062827);
INSERT INTO cities VALUES (22,"Dhaka","Bangladesh",6493177,23.7230556,90.4086111);
INSERT INTO cities VALUES (23,"Lahore","Pakistan",6312576,31.549722,74.343611);
INSERT INTO cities VALUES (24,"Rio de Janeiro","Brazil",6023742,-22.9,-43.233333);
INSERT INTO cities VALUES (25,"Baghdad","Iraq",5672516,33.3386111,44.3938889);
INSERT INTO cities VALUES (26,"Bangkok","Thailand",5104475,13.753979,100.501444);
INSERT INTO cities VALUES (27,"Bangalore","India",4931603,12.983333,77.583333);
INSERT INTO cities VALUES (28,"Santiago","Chile",4837248,-33.45,-70.666667);
INSERT INTO cities VALUES (29,"Calcutta","India",4631819,22.569722,88.369722);
INSERT INTO cities VALUES (30,"Toronto","Canada",4612187,43.666667,-79.416667);
INSERT INTO cities VALUES (31,"Rangoon","Myanmar",4477782,16.7833333,96.1666667);
INSERT INTO cities VALUES (32,"Sydney","Australia",4394585,-33.861481,151.205475);
INSERT INTO cities VALUES (33,"Madras","India",4328416,13.083333,80.283333);
INSERT INTO cities VALUES (34,"Wuhan","China",4184206,30.580125,114.273405);
INSERT INTO cities VALUES (35,"Saint Petersburg","Russian Federation",4039751,59.894444,30.264167);
INSERT INTO cities VALUES (36,"Chongqing","China",3967028,29.562778,106.552778);
INSERT INTO cities VALUES (37,"Xian","China",3953191,34.258333,108.928611);
INSERT INTO cities VALUES (38,"Chengdu","China",3950437,30.666667,104.066667);
INSERT INTO cities VALUES (39,"Los Angeles","United States",3877129,34.0522222,-118.2427778);
INSERT INTO cities VALUES (40,"Alexandria","Egypt",3811512,31.1980556,29.9191667);
INSERT INTO cities VALUES (41,"Tianjin","China",3766207,39.142222,117.176667);
INSERT INTO cities VALUES (42,"Melbourne","Australia",3730212,-37.813938,144.963425);
INSERT INTO cities VALUES (43,"Ahmadabad","India",3719933,23.033333,72.616667);
INSERT INTO cities VALUES (44,"Abidjan","Côte d'Ivoire",3692570,5.309657,-4.012656);
INSERT INTO cities VALUES (45,"Kano","Nigeria",3626204,11.994354,8.513807);
INSERT INTO cities VALUES (46,"Casablanca","Morocco",3609698,33.592779,-7.619157);
INSERT INTO cities VALUES (47,"Hyderabad","India",3598199,17.375278,78.474444);
INSERT INTO cities VALUES (48,"Ibadan","Nigeria",3565810,7.387778,3.896389);
INSERT INTO cities VALUES (49,"Singapore","Singapore",3547809,1.2930556,103.8558333);
INSERT INTO cities VALUES (50,"Ankara","Turkey",3519177,39.911652,32.840305);
INSERT INTO cities VALUES (51,"Shenyang","China",3512192,41.792222,123.432778);
INSERT INTO cities VALUES (52,"Riyadh","Saudi Arabia",3469290,24.653664,46.71522);
INSERT INTO cities VALUES (53,"Ho Chi Minh City","Viet Nam",3467426,10.75,106.666667);
INSERT INTO cities VALUES (54,"Cape Town","South Africa",3433504,-33.925839,18.423218);
INSERT INTO cities VALUES (55,"Berlin","Germany",3398362,52.516667,13.4);
INSERT INTO cities VALUES (56,"Montreal","Canada",3268513,45.5,-73.583333);
INSERT INTO cities VALUES (57,"Harbin","China",3229883,45.75,126.65);
INSERT INTO cities VALUES (58,"Guangzhou","China",3152825,23.116667,113.25);
INSERT INTO cities VALUES (59,"Durban","South Africa",3120340,-29.857896,31.029198);
INSERT INTO cities VALUES (60,"Madrid","Spain",3102644,40.408566,-3.69222);
INSERT INTO cities VALUES (61,"Nanjing","China",3087010,32.061667,118.777778);
INSERT INTO cities VALUES (62,"Kabul","Afghanistan",3043589,34.516667,69.183333);
INSERT INTO cities VALUES (63,"Pune","India",2935968,18.533333,73.866667);
INSERT INTO cities VALUES (64,"Surat","India",2894675,20.966667,72.9);
INSERT INTO cities VALUES (65,"Chicago","United States",2841952,41.85,-87.65);
INSERT INTO cities VALUES (66,"Kanpur","India",2823523,26.466667,80.35);
INSERT INTO cities VALUES (67,"Umm Durman","Sudan",2810328,15.6361111,32.4372222);
INSERT INTO cities VALUES (68,"Luanda","Angola",2776125,-8.836804,13.233174);
INSERT INTO cities VALUES (69,"Addis Abeba","Ethiopia",2757807,9.024325,38.749226);
INSERT INTO cities VALUES (70,"Nairobi","Kenya",2750561,-1.2833333,36.8166667);
INSERT INTO cities VALUES (71,"Taiyuan","China",2722475,37.726944,112.470833);
INSERT INTO cities VALUES (72,"Jaipur","India",2711937,26.916667,75.816667);
INSERT INTO cities VALUES (73,"Salvador","Brazil",2711903,-12.983333,-38.516667);
INSERT INTO cities VALUES (74,"Dakar","Senegal",2702820,14.6708333,-17.4380556);
INSERT INTO cities VALUES (75,"Dar es Salaam","United Republic of Tanzania",2698651,-6.8,39.2833333);
INSERT INTO cities VALUES (76,"Rome","Italy",2643736,41.9,12.483333);
INSERT INTO cities VALUES (77,"Mogadishu","Somalia",2590180,2.0666667,45.3666667);
INSERT INTO cities VALUES (78,"Jiddah","Saudi Arabia",2545728,21.516944,39.219167);
INSERT INTO cities VALUES (79,"Changchun","China",2537421,43.88,125.322778);
INSERT INTO cities VALUES (80,"Taipei","Taiwan",2514794,25.0391667,121.525);
INSERT INTO cities VALUES (81,"Kiev","Ukraine",2514227,50.433333,30.516667);
INSERT INTO cities VALUES (82,"Faisalabad","Pakistan",2507302,31.416667,73.083333);
INSERT INTO cities VALUES (83,"Izmir","Turkey",2501895,38.412726,27.138376);
INSERT INTO cities VALUES (84,"Lakhnau","India",2472250,26.85,80.916667);
INSERT INTO cities VALUES (85,"Gizeh","Egypt",2443490,30.0086111,31.2122222);
INSERT INTO cities VALUES (86,"Fortaleza","Brazil",2416920,-3.316667,-41.416667);
INSERT INTO cities VALUES (87,"Cali","Colombia",2392897,3.437222,-76.5225);
INSERT INTO cities VALUES (88,"Surabaya","Indonesia",2374920,-7.249167,112.750833);
INSERT INTO cities VALUES (89,"Belo Horizonte","Brazil",2373255,-19.916667,-43.933333);
INSERT INTO cities VALUES (90,"Mashhad","Iran",2307254,36.297,59.6062);
INSERT INTO cities VALUES (91,"Nagpur","India",2228191,21.15,79.1);
INSERT INTO cities VALUES (92,"Harare","Zimbabwe",2213701,-17.8177778,31.0447222);
INSERT INTO cities VALUES (93,"Brasília","Brazil",2207812,-15.783333,-47.916667);
INSERT INTO cities VALUES (94,"Santo Domingo","Dominican Republic",2202016,18.4666667,-69.9);
INSERT INTO cities VALUES (95,"Nagoya","Japan",2191291,35.180198,136.906739);
INSERT INTO cities VALUES (96,"Aleppo","Syrian Arab Republic",2139878,36.2027778,37.1586111);
INSERT INTO cities VALUES (97,"Paris","France",2110694,48.866667,2.333333);
INSERT INTO cities VALUES (98,"Jinan","China",2069266,36.668333,116.997222);
INSERT INTO cities VALUES (99,"Tangshan","China",2054526,37.3325,114.701389);
INSERT INTO cities VALUES (100,"Dalian","China",2035307,38.912222,121.602222);
INSERT INTO cities VALUES (101,"Houston","United States",2027712,29.7630556,-95.3630556);
INSERT INTO cities VALUES (102,"Johannesburg","South Africa",2026466,-26.205171,28.049815);
INSERT INTO cities VALUES (103,"Medellín","Colombia",2000001,6.25184,-75.563591);
INSERT INTO cities VALUES (104,"Algiers","Algeria",1980242,36.7630556,3.0505556);
INSERT INTO cities VALUES (105,"Tashkent","Uzbekistan",1978078,41.3166667,69.25);
INSERT INTO cities VALUES (106,"Khartoum","Sudan",1974780,15.5880556,32.5341667);
INSERT INTO cities VALUES (107,"Accra","Ghana",1963460,5.55,-0.2166667);
INSERT INTO cities VALUES (108,"Guayaquil","Ecuador",1952029,-2.1666667,-79.9);
INSERT INTO cities VALUES (109,"Maracaibo","Venezuela",1948269,10.6316667,-71.6405556);
INSERT INTO cities VALUES (110,"Rabat","Morocco",1894386,34.013784,-6.844268);
INSERT INTO cities VALUES (111,"Jilin","China",1881977,43.850833,126.560278);
INSERT INTO cities VALUES (112,"Hangzhou","China",1878129,30.29365,120.161419);
INSERT INTO cities VALUES (113,"Bucharest","Romania",1877155,44.433333,26.1);
INSERT INTO cities VALUES (114,"Nanchang","China",1871351,28.55,115.933333);
INSERT INTO cities VALUES (115,"Conakry","Guinea",1871185,9.5091667,-13.7122222);
INSERT INTO cities VALUES (116,"Brisbane","Australia",1843459,-27.47101,153.024292);
INSERT INTO cities VALUES (117,"Vancouver","Canada",1837970,49.25,-123.133333);
INSERT INTO cities VALUES (118,"Indore","India",1837230,22.716667,75.833333);
INSERT INTO cities VALUES (119,"Caracas","Venezuela",1815681,10.5,-66.9166667);
INSERT INTO cities VALUES (120,"Ecatepec","Mexico",1806297,19.601111,-99.0525);
INSERT INTO cities VALUES (121,"Medan","Indonesia",1751190,3.583333,98.666667);
INSERT INTO cities VALUES (122,"Rawalpindi","Pakistan",1743599,33.6007,73.0679);
INSERT INTO cities VALUES (123,"Minsk","Belarus",1742123,53.9,27.5666667);
INSERT INTO cities VALUES (124,"Hamburg","Germany",1733846,53.55,10);
INSERT INTO cities VALUES (125,"Curitiba","Brazil",1718433,-25.416667,-49.25);
INSERT INTO cities VALUES (126,"Budapest","Hungary",1708088,47.5,19.083333);
INSERT INTO cities VALUES (127,"Bandung","Indonesia",1699822,-6.903889,107.618611);
INSERT INTO cities VALUES (128,"Soweto","South Africa",1695094,-26.267812,27.858492);
INSERT INTO cities VALUES (129,"Warsaw","Poland",1651676,52.25,21);
INSERT INTO cities VALUES (130,"Qingdao","China",1642245,36.098611,120.371944);
INSERT INTO cities VALUES (131,"Guadalajara","Mexico",1640649,20.666667,-103.333333);
INSERT INTO cities VALUES (132,"Pretoria","South Africa",1619485,-25.706944,28.229444);
INSERT INTO cities VALUES (133,"Patna","India",1600085,25.6,85.116667);
INSERT INTO cities VALUES (134,"Bhopal","India",1600080,23.266667,77.4);
INSERT INTO cities VALUES (135,"Manaus","Brazil",1598227,-3.113333,-60.025278);
INSERT INTO cities VALUES (136,"Xinyang","China",1590668,32.095833,114.120278);
INSERT INTO cities VALUES (137,"Kaduna","Nigeria",1582211,10.522239,7.43828);
INSERT INTO cities VALUES (138,"Damascus","Syrian Arab Republic",1576843,33.5,36.3);
INSERT INTO cities VALUES (139,"Phnum Pénh","Cambodia",1573523,11.55,104.9166667);
INSERT INTO cities VALUES (140,"Barcelona","Spain",1570378,41.398371,2.1741);
INSERT INTO cities VALUES (141,"Vienna","Austria",1569315,48.2,16.366667);
INSERT INTO cities VALUES (142,"Esfahan","Iran",1547241,32.657218,51.677608);
INSERT INTO cities VALUES (143,"Ludhiana","India",1545476,30.9,75.85);
INSERT INTO cities VALUES (144,"Kobe","Japan",1528487,34.691304,135.182995);
INSERT INTO cities VALUES (145,"Bekasi","Indonesia",1520204,-6.2349,106.9896);
INSERT INTO cities VALUES (146,"Kaohsiung","Taiwan",1512832,22.6177778,120.3013889);
INSERT INTO cities VALUES (147,"Kaohsiung","Taiwan",1512832,22.6333333,120.35);
INSERT INTO cities VALUES (148,"Ürümqi","China",1508225,43.800965,87.600459);
INSERT INTO cities VALUES (149,"Thana","India",1486453,19.2,72.966667);
INSERT INTO cities VALUES (150,"Recife","Brazil",1478118,-8.05,-34.9);
INSERT INTO cities VALUES (151,"Kumasi","Ghana",1468797,6.6833333,-1.6166667);
INSERT INTO cities VALUES (152,"Kuala Lumpur","Malaysia",1453978,3.166667,101.7);
INSERT INTO cities VALUES (153,"Philadelphia","United States",1453268,39.9522222,-75.1641667);
INSERT INTO cities VALUES (154,"Karaj","Iran",1448996,35.8355,51.0103);
INSERT INTO cities VALUES (155,"Perth","Australia",1446715,-31.95224,115.861397);
INSERT INTO cities VALUES (156,"Cordoba","Argentina",1441007,-31.413496,-64.181052);
INSERT INTO cities VALUES (157,"Multan","Pakistan",1437644,30.195556,71.475278);
INSERT INTO cities VALUES (158,"Hanoi","Viet Nam",1431377,21.033333,105.85);
INSERT INTO cities VALUES (159,"Agra","India",1430194,27.183333,78.016667);
INSERT INTO cities VALUES (160,"Phoenix","United States",1428509,33.4483333,-112.0733333);
INSERT INTO cities VALUES (161,"Tabriz","Iran",1424701,38.08,46.2919);
INSERT INTO cities VALUES (162,"Novosibirsk","Russian Federation",1419016,55.0415,82.9346);
INSERT INTO cities VALUES (163,"Lanzhou","China",1417742,36.056389,103.792222);
INSERT INTO cities VALUES (164,"Bursa","Turkey",1413485,40.191667,29.061111);
INSERT INTO cities VALUES (165,"Vadodara","India",1409565,22.3,73.2);
INSERT INTO cities VALUES (166,"Belém","Brazil",1407737,-1.45,-48.483333);
INSERT INTO cities VALUES (167,"Juarez","Mexico",1403025,31.733333,-106.483333);
INSERT INTO cities VALUES (168,"Fushun","China",1400646,41.7,123.883333);
INSERT INTO cities VALUES (169,"Quito","Ecuador",1399814,-0.2166667,-78.5);
INSERT INTO cities VALUES (170,"Puebla","Mexico",1392137,19.05,-98.2);
INSERT INTO cities VALUES (171,"Antananarivo","Madagascar",1391506,-18.9166667,47.5166667);
INSERT INTO cities VALUES (172,"Luoyang","China",1390581,34.683611,112.453611);
INSERT INTO cities VALUES (173,"Hefei","China",1388904,31.863889,117.280833);
INSERT INTO cities VALUES (174,"Hyderabad","Pakistan",1386840,25.39242,68.373656);
INSERT INTO cities VALUES (175,"Valencia","Venezuela",1385202,10.1805556,-68.0038889);
INSERT INTO cities VALUES (176,"Gujranwala","Pakistan",1384869,32.161667,74.188309);
INSERT INTO cities VALUES (177,"Barranquilla","Colombia",1380437,10.963889,-74.796389);
INSERT INTO cities VALUES (178,"Tijuana","Mexico",1376494,32.533333,-117.016667);
INSERT INTO cities VALUES (179,"Lubumbashi","Democratic Republic of the Congo",1374808,-11.666667,27.466667);
INSERT INTO cities VALUES (180,"Porto Alegre","Brazil",1372763,-30.033333,-51.2);
INSERT INTO cities VALUES (181,"Tangerang","Indonesia",1372185,-6.178056,106.63);
INSERT INTO cities VALUES (182,"Handan","China",1358318,36.566667,114.533333);
INSERT INTO cities VALUES (183,"Kampala","Uganda",1353236,0.3155556,32.5655556);
INSERT INTO cities VALUES (184,"Suzhou","China",1343091,31.311389,120.618056);
INSERT INTO cities VALUES (185,"Khulna","Bangladesh",1342429,22.8,89.55);
INSERT INTO cities VALUES (186,"Douala","Cameroon",1338144,4.0502778,9.7);
INSERT INTO cities VALUES (187,"Makasar","Indonesia",1321832,-5.14,119.4221);
INSERT INTO cities VALUES (188,"Kawasaki","Japan",1306804,35.520556,139.717222);
INSERT INTO cities VALUES (189,"Montevideo","Uruguay",1305865,-34.8580556,-56.1708333);
INSERT INTO cities VALUES (190,"Yaoundé","Cameroon",1299446,3.8666667,11.5166667);
INSERT INTO cities VALUES (191,"Bamako","Mali",1297390,12.65,-8);
INSERT INTO cities VALUES (192,"Semarang","Indonesia",1288221,-6.9932,110.4203);
INSERT INTO cities VALUES (193,"Yekaterinburg","Russian Federation",1287586,56.8519,60.6122);
INSERT INTO cities VALUES (194,"San Diego","United States",1287050,32.7152778,-117.1563889);
INSERT INTO cities VALUES (195,"Pimpri","India",1284702,18.616667,73.8);
INSERT INTO cities VALUES (196,"Nizhniy Novgorod","Russian Federation",1284176,56.326944,44.0075);
INSERT INTO cities VALUES (197,"Faridabad","India",1280909,28.433333,77.316667);
INSERT INTO cities VALUES (198,"Lusaka","Zambia",1267458,-15.4166667,28.2833333);
INSERT INTO cities VALUES (199,"Kalyan","India",1262355,19.25,73.15);
INSERT INTO cities VALUES (200,"San Antonio","United States",1256810,29.4238889,-98.4933333);
INSERT INTO cities VALUES (201,"Stockholm","Sweden",1253309,59.333333,18.05);
INSERT INTO cities VALUES (202,"Beirut","Lebanon",1252010,33.8719444,35.5097222);
INSERT INTO cities VALUES (203,"Shiraz","Iran",1249972,29.6036,52.5388);
INSERT INTO cities VALUES (204,"Adana","Turkey",1249680,37.001667,35.328889);
INSERT INTO cities VALUES (205,"Munich","Germany",1246133,48.15,11.5833);
INSERT INTO cities VALUES (206,"Palembang","Indonesia",1241334,-2.916667,104.75);
INSERT INTO cities VALUES (207,"Port-au-Prince","Haiti",1234750,18.5391667,-72.335);
INSERT INTO cities VALUES (208,"Nezahualcóyotl","Mexico",1232565,19.413611,-99.033056);
INSERT INTO cities VALUES (209,"Peshawar","Pakistan",1219248,34.008,71.578488);
INSERT INTO cities VALUES (210,"Rosario","Argentina",1218497,-32.946818,-60.639317);
INSERT INTO cities VALUES (211,"Davao","Philippines",1212285,7.073056,125.612778);
INSERT INTO cities VALUES (212,"Dallas","United States",1211704,32.7833333,-96.8);
INSERT INTO cities VALUES (213,"Mandalay","Myanmar",1208227,22,96.0833333);
INSERT INTO cities VALUES (214,"Almaty","Kazakhstan",1204820,43.25,76.95);
INSERT INTO cities VALUES (215,"Mecca","Saudi Arabia",1199659,21.426667,39.826111);
INSERT INTO cities VALUES (216,"Ghaziabad","India",1199306,28.666667,77.433333);
INSERT INTO cities VALUES (217,"Anshan","China",1199275,41.123611,122.99);
INSERT INTO cities VALUES (218,"Xuzhou","China",1199193,34.180454,117.15707);
INSERT INTO cities VALUES (219,"Depok","Indonesia",1198195,-6.343333,106.498889);
INSERT INTO cities VALUES (220,"Maputo","Mozambique",1191613,-25.9652778,32.5891667);
INSERT INTO cities VALUES (221,"Freetown","Sierra Leone",1190761,8.49,-13.2341667);
INSERT INTO cities VALUES (222,"Fuzhou","China",1179720,26.061389,119.306111);
INSERT INTO cities VALUES (223,"Rajkot","India",1177434,22.3,70.783333);
INSERT INTO cities VALUES (224,"Guiyang","China",1171633,26.583333,106.716667);
INSERT INTO cities VALUES (225,"Goiânia","Brazil",1171209,-16.666667,-49.266667);
INSERT INTO cities VALUES (226,"Guarulhos","Brazil",1169601,-23.45068,-46.526175);
INSERT INTO cities VALUES (227,"Varanasi","India",1164520,25.333333,83);
INSERT INTO cities VALUES (228,"Fez","Morocco",1160239,34.037151,-4.999797);
INSERT INTO cities VALUES (229,"Milan","Italy",1156903,45.466667,9.2);
INSERT INTO cities VALUES (230,"Prague","Czech Republic",1154508,50.083333,14.466667);
INSERT INTO cities VALUES (231,"Tripoli","Libya",1150990,32.8925,13.18);
INSERT INTO cities VALUES (232,"Port Harcourt","Nigeria",1148753,4.777423,7.013404);
INSERT INTO cities VALUES (233,"Hiroshima","Japan",1143850,34.434844,132.740622);
INSERT INTO cities VALUES (234,"Managua","Nicaragua",1140499,12.1508333,-86.2683333);
INSERT INTO cities VALUES (235,"Dubai","United Arab Emirates",1137376,25.258172,55.304717);
INSERT INTO cities VALUES (236,"Samara","Russian Federation",1134742,53.1835,50.1182);
INSERT INTO cities VALUES (237,"Omsk","Russian Federation",1129289,55,73.4);
INSERT INTO cities VALUES (238,"Bénin","Nigeria",1125126,6.335045,5.627492);
INSERT INTO cities VALUES (239,"Monterrey","Mexico",1122912,25.666667,-100.316667);
INSERT INTO cities VALUES (240,"Baku","Azerbaijan",1116513,40.395278,49.882222);
INSERT INTO cities VALUES (241,"Brazzaville","Congo",1115773,-4.2591667,15.2847222);
INSERT INTO cities VALUES (242,"Belgrade","Serbia",1115200,44.818611,20.468056);
INSERT INTO cities VALUES (243,"León","Mexico",1114662,21.116667,-101.666667);
INSERT INTO cities VALUES (244,"Maiduguri","Nigeria",1112511,11.846441,13.160274);
INSERT INTO cities VALUES (245,"Wuxi","China",1108647,31.568873,120.288573);
INSERT INTO cities VALUES (246,"Kazan","Russian Federation",1104750,55.78874,49.122144);
INSERT INTO cities VALUES (247,"Yerevan","Armenia",1093499,40.1811111,44.5136111);
INSERT INTO cities VALUES (248,"Amritsar","India",1092528,31.633056,74.865556);
INSERT INTO cities VALUES (249,"Copenhagen","Denmark",1089958,55.666667,12.583333);
INSERT INTO cities VALUES (250,"Taichung","Taiwan",1083582,24.1433333,120.6813889);
INSERT INTO cities VALUES (251,"Saitama","Japan",1077730,35.895534,139.67747);
INSERT INTO cities VALUES (252,"Rostov-na-Donu","Russian Federation",1074495,47.23135,39.723284);
INSERT INTO cities VALUES (253,"Adelaide","Australia",1074168,-34.928661,138.598633);
INSERT INTO cities VALUES (254,"Allahabad","India",1073544,25.45,81.85);
INSERT INTO cities VALUES (255,"Gaziantep","Turkey",1066561,37.059444,37.3825);
INSERT INTO cities VALUES (256,"Visakhapatnam","India",1063293,17.7,83.3);
INSERT INTO cities VALUES (257,"Chelyabinsk","Russian Federation",1062931,55.154444,61.429722);
INSERT INTO cities VALUES (258,"Sofia","Bulgaria",1062065,42.6833333,23.3166667);
INSERT INTO cities VALUES (259,"Datong","China",1052678,40.093611,113.291389);
INSERT INTO cities VALUES (260,"Tbilisi","Georgia",1049516,41.725,44.7908333);
INSERT INTO cities VALUES (261,"Xianyang","China",1034081,34.33778,108.70261);
INSERT INTO cities VALUES (262,"Ufa","Russian Federation",1033350,54.785167,56.045621);
INSERT INTO cities VALUES (263,"Campinas","Brazil",1031573,-22.9,-47.083333);
INSERT INTO cities VALUES (264,"Ouagadougou","Burkina Faso",1031330,12.3702778,-1.5247222);
INSERT INTO cities VALUES (265,"Jabalpur","India",1030276,23.166667,79.95);
INSERT INTO cities VALUES (266,"Haora","India",1027771,22.589167,88.310278);
INSERT INTO cities VALUES (267,"Huainan","China",1027655,32.626389,116.996944);
INSERT INTO cities VALUES (268,"Dublin","Ireland",1024027,53.3330556,-6.2488889);
INSERT INTO cities VALUES (269,"Kunming","China",1023674,25.038889,102.718333);
INSERT INTO cities VALUES (270,"Brussels","Belgium",1019022,50.833333,4.333333);
INSERT INTO cities VALUES (271,"Aurangabad","India",1016520,19.883333,75.333333);
INSERT INTO cities VALUES (272,"Qom","Iran",1011103,34.6401,50.8764);
INSERT INTO cities VALUES (273,"Volgograd","Russian Federation",1010331,48.71939,44.501835);
INSERT INTO cities VALUES (274,"Shenzhen","China",1002592,22.533333,114.133333);
INSERT INTO cities VALUES (275,"Nova Iguaçu","Brazil",1002129,-22.759188,-43.431887);
INSERT INTO cities VALUES (276,"Rongcheng","China",1001985,23.528858,116.364159);
INSERT INTO cities VALUES (277,"Odesa","Ukraine",1001553,46.463934,30.738551);

END TRANSACTION;
