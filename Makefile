build:
	sbt compile
	sbt pack
	cp -R data/ target/pack
	cp -R db/ target/pack

link:
	rm main
	ln -s target/pack/bin/main main
	@echo "Project package in target/pack"
	@echo "Run with ./main [ARGS]"

clean:
	sbt clean

test:
	sbt test
