# Traveling Salesman Problem.

Implementation of Simulated annealing as an approach to solve TSP.

## Dependencies

- Scala v2.11.8
- Scala Build Tool (sbt) v0.13.12
- SQLite3 v3.14.2

## Usage
- ``make build`` to build the project
- ``make test`` to run some tests
- ``make link`` to link executable
- ``make clean`` to clean the project
- ``sh plot.sh [file]`` to plot output

### Example
1. ``$ make build ``
2. ``$ make link ``
3. ``$ ./main 69 100 --input cities --output cost -b``
4. ``$ sh plot.sh cost``
